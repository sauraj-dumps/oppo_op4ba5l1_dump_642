#! /system/bin/sh
#***********************************************************
#** Copyright (C), 2008-2020, OPPO Mobile Comm Corp., Ltd.
#** OPLUS_FEATURE_BT_HCI_LOG
#**
#** Version: 1.0
#** Date : 2020/06/06
#** Author: Laixin@CONNECTIVITY.BT.BASIC.LOG.70745, 2020/06/06
#** Add for: cached bt hci log and feedback
#**
#** ---------------------Revision History: ---------------------
#**  <author>    <data>       <version >       <desc>
#**  Laixin    2020/06/06     1.0        build this module
#****************************************************************/

config="$1"

function uploadBtSSRDump() {
    BT_DUMP_PATH=/data/vendor/ssrdump/
    DCS_BT_LOG_PATH=/data/oppo/log/DCS/de/network_logs/bt_fw_dump
    if [ ! -d ${DCS_BT_LOG_PATH} ];then
        mkdir -p ${DCS_BT_LOG_PATH}
    fi
    #chown -R system:system ${DCS_BT_LOG_PATH}
    #chmod -R 777 ${BT_DUMP_PATH}

    zip_name=`getprop persist.sys.bluetooth.dump.zip.name`

    chmod 777 ${DCS_BT_LOG_PATH}/*
    debtssrdumpcount=`ls -l /data/oppo/log/DCS/de/network_logs/bt_fw_dump  | grep "^-" | wc -l`
    enbtssrdumpcount=`ls -l /data/oppo/log/DCS/en/network_logs/bt_fw_dump  | grep "^-" | wc -l`
    if [ $debtssrdumpcount -lt 10 ] && [ $enbtssrdumpcount -lt 10 ];then
        tar -czvf  ${DCS_BT_LOG_PATH}/${zip_name}.tar.gz --exclude=*.tar.gz -C ${DCS_BT_LOG_PATH} ${DCS_BT_LOG_PATH}
    fi
    #sleep 5
    if [ "w${DCS_BT_LOG_PATH}" != "w" ];then
        rm ${DCS_BT_LOG_PATH}/*.log
        rm ${DCS_BT_LOG_PATH}/*.cfa
        rm ${DCS_BT_LOG_PATH}/*.bin
    fi

    chown system:system ${DCS_BT_LOG_PATH}/${zip_name}.tar.gz
    chmod 777 ${DCS_BT_LOG_PATH}/${zip_name}.tar.gz

    setprop sys.oplus.bt.collect_bt_ssrdump 0
}

case "$config" in
        "uploadBtSSRDump")
        uploadBtSSRDump
    ;;
esac
