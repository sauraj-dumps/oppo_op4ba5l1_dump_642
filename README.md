## OP4BA5L1
- Manufacturer: oppo
- Platform: atoll
- Codename: OP4BA5L1
- Brand: OPPO
- Flavor: qssi-user
- Release Version: 10
- Id: QKQ1.200428.002
- Incremental: 
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 
- OTA version: CPH2113EU_11.A.28_0280_202012081134
- Branch: CPH2113EU_11.A.28_0280_202012081134
- Repo: oppo_op4ba5l1_dump_642


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
