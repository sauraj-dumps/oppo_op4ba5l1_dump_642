#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:100663296:8aa217a818a7193321bc44e64ebe0471fac20af7; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:569de2527f021f49556a55333c0e81ea987197c2 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:100663296:8aa217a818a7193321bc44e64ebe0471fac20af7 && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.recovery.updated true
fi
