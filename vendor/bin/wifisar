#!/vendor/bin/sh

SAR_EXEC_FILE="/vendor/bin/vendor_cmd_tool"
SAR_CFG_FILE="/vendor/etc/wifi/sar-vendor-cmd.xml"


WIFI_SAR_DISABLE_CMD_BODY="--ENABLE 5 --NUM_SPECS 0 --END_CMD"

# Note element rule:
# 1. --NUM_SPECS should equal exactly item number;
# 2. Please not modify exist rules, just add new rule for specific country/reg,
#    for add new rules, you should be carefuly about below:
# 	1) Notice BAND\CHAIN\MOD info;
# 	2) Notice POW, the unit is 0.5dBm;
# 	3) Notice always check NUM_SPECS value, it MUST equal item numbers.
# 3. Rule 0 is an example, you should NEVER modify it;
# 4. sarIdx should match enum "SwitchWifiSar" index defined in OemProximitySensorManager.java.
WIFI_SAR_SETTING_19365=(
## get detail information from 80-ya512-12_yd_wcn39xx_wlan_coexistence_software_user_guide.pdf
## sarIdx:0 -- 2.4G/5G both (example, should NEVER use/modify it)
"--ENABLE 6 --NUM_SPECS 4 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 28 --END_ATTR \
--NESTED_AUTO --BAND 0 --CHAIN 1 --POW 28 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 28 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 28 --END_ATTR \
--END_ATTR --END_CMD"

###################### Europe ######################
#
## sarIdx:1 -- For 2.4g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:2 -- -- For 2.4g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:3 -- For 5g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 32 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:4 -- For 5g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 25 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:5 -- For 2.4g+5g+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 29 --END_ATTR \
--NESTED_AUTO --BAND 1 --POW 26 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:6 -- For 2.4g+5g+body
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 31 --END_ATTR \
--NESTED_AUTO --BAND 1 --POW 20 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:7 -- For 2.4g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 27 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:8 -- For 2.4g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 26 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:9 -- For 5g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 24 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:10 -- For 5g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 15 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:11 -- For 2.4g+5g+modem+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 21 --END_ATTR \
--NESTED_AUTO --BAND 1 --POW 18 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:12 -- For 2.4g+5g+modem+body
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 22 --END_ATTR \
--NESTED_AUTO --BAND 1 --POW 11 --END_ATTR \
--END_ATTR --END_CMD"

###################### CE ######################
#
## sarIdx:13 -- For 2.4g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:14 -- -- For 2.4g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:15 -- For 5g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 32 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:16 -- For 5g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 32 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:17 -- For 2.4g+5g+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 33 --END_ATTR \
--NESTED_AUTO --BAND 1 --POW 29 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:18 -- For 2.4g+5g+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 24 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:19 -- For 2.4g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 30 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:20 -- For 2.4g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:21 -- For 5g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 27 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:22 -- For 5g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 22 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:23 -- For 2.4g+5g+modem+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 24 --END_ATTR \
--NESTED_AUTO --BAND 1 --POW 22 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:24 -- For 2.4g+5g+modem+body
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 28 --END_ATTR \
--NESTED_AUTO --BAND 1 --POW 14 --END_ATTR \
--END_ATTR --END_CMD"

###################### FCC ######################
#
## sarIdx:25 -- For 2.4g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 22 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:26 -- -- For 2.4g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 40 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:27 -- For 5g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 19 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:28 -- For 5g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 40 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:29 -- For 2.4g+5g+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 18 --END_ATTR \
--NESTED_AUTO --BAND 1 --POW 15 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:30 -- For 2.4g+5g+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 40--END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:31 -- For 2.4g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 15 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:32 -- For 2.4g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 40 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:33 -- For 5g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 13 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:34 -- For 5g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 24 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:35 -- For 2.4g+5g+modem+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 14 --END_ATTR \
--NESTED_AUTO --BAND 1 --POW 11 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:36 -- For 2.4g+5g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 24 --END_ATTR \
--END_ATTR --END_CMD"

###################### INDIA ######################
#
## sarIdx:37 -- For 2.4g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 32 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:38 -- -- For 2.4g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:39 -- For 5g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 32 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:40 -- For 5g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:41 -- For 2.4g+5g+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 32 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:42 -- For 2.4g+5g+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:43 -- For 2.4g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 26 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:44 -- For 2.4g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:45 -- For 5g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 26 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:46 -- For 5g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:47 -- For 2.4g+5g+modem+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 26 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:48 -- For 2.4g+5g+modem+body
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 36 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"
)

WIFI_SAR_SETTING_19567=(
## get detail information from 80-ya512-12_yd_wcn39xx_wlan_coexistence_software_user_guide.pdf
## sarIdx:0 -- 2.4G/5G both (example, should NEVER use/modify it)
"--ENABLE 6 --NUM_SPECS 4 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 28 --END_ATTR \
--NESTED_AUTO --BAND 0 --CHAIN 1 --POW 28 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 28 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 28 --END_ATTR \
--END_ATTR --END_CMD"

###################### Europe ######################
#
## sarIdx:1 -- For 2.4g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:2 -- -- For 2.4g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:3 -- For 5g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:4 -- For 5g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:5 -- For 2.4g+5g+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 36 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:6 -- For 2.4g+5g+body
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 36 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:7 -- For 2.4g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 32 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:8 -- For 2.4g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:9 -- For 5g+modem+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 36 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:10 -- For 5g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 18 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:11 -- For 2.4g+5g+modem+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 36 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:12 -- For 2.4g+5g+modem+body
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 36 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

###################### CE ######################
#
## sarIdx:13 -- For 2.4g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:14 -- -- For 2.4g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:15 -- For 5g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:16 -- For 5g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:17 -- For 2.4g+5g+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 36 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:18 -- For 2.4g+5g+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:19 -- For 2.4g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 24 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:20 -- For 2.4g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 30 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:21 -- For 5g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 20 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:22 -- For 5g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 30 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:23 -- For 2.4g+5g+modem+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 28 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:24 -- For 2.4g+5g+modem+body
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 36 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

###################### FCC ######################
#
## sarIdx:25 -- For 2.4g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 26 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:26 -- -- For 2.4g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 40 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:27 -- For 5g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 17 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:28 -- For 5g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 40 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:29 -- For 2.4g+5g+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 28 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 19 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:30 -- For 2.4g+5g+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 28 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:31 -- For 2.4g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 22 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:32 -- For 2.4g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 31 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:33 -- For 5g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 13 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:34 -- For 5g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 26 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:35 -- For 2.4g+5g+modem+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 22 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 13 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:36 -- For 2.4g+5g+modem+body
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 31 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 1 --POW 26 --END_ATTR \
--END_ATTR --END_CMD"

###################### INDIA ######################
#
## sarIdx:37 -- For 2.4g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 28 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:38 -- -- For 2.4g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:39 -- For 5g only+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 22 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:40 -- For 5g only+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:41 -- For 2.4g+5g+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 28 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 22 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:42 -- For 2.4g+5g+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:43 -- For 2.4g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 22 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:44 -- For 2.4g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 36 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:45 -- For 5g+modem+head
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 16 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:46 -- For 5g+modem+body
"--ENABLE 6 --NUM_SPECS 1 \
--SAR_SPEC \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:47 -- For 2.4g+5g+modem+head
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 22 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 18 --END_ATTR \
--END_ATTR --END_CMD"

## sarIdx:48 -- For 2.4g+5g+modem+body
"--ENABLE 6 --NUM_SPECS 2 \
--SAR_SPEC \
--NESTED_AUTO --BAND 0 --CHAIN 0 --POW 36 --END_ATTR \
--NESTED_AUTO --BAND 1 --CHAIN 0 --POW 34 --END_ATTR \
--END_ATTR --END_CMD"
)


# temp static value, should get from sysfs based on STA/SAP on/off status
# for android O, interface of SAP is softap0
get_wifi_iface() {
	echo "wlan0"
}

build_cmd_header() {
	iface="$(get_wifi_iface)"
	echo "-f $SAR_CFG_FILE -i $iface --START_CMD --SAR_SET"
}

build_cmd_body() {
	sarIdx="$1"
	prjVer="$2"

	# disable sar cmd body
	[ "0" -eq "$sarIdx" ] && echo "$WIFI_SAR_DISABLE_CMD_BODY" && return

	case "$prjVer" in
		"19365" | "19367" | "19368")
			arraySize=${#WIFI_SAR_SETTING_19365[@]}
			if [ "$sarIdx" -lt "$arraySize" ]; then
				echo "${WIFI_SAR_SETTING_19365[$sarIdx]}"
				return
			fi
			;;
		"19567" | "19568" | "19569")
			arraySize=${#WIFI_SAR_SETTING_19567[@]}
			if [ "$sarIdx" -lt "$arraySize" ]; then
				echo "${WIFI_SAR_SETTING_19567[$sarIdx]}"
				return
			fi
			;;
		*)
			echo "Project $prjVer not support now!!" > /dev/null
			;;
	esac

	# Normally we will never goto here, if process goes wrong, always disable sar.
	echo "$WIFI_SAR_DISABLE_CMD_BODY"
}

build_cmd() {
	cHdr="$(build_cmd_header)"
	cBody="$(build_cmd_body $1 $2)"

	echo "$cHdr $cBody"
}

apply_sar() {
	cmd="$(build_cmd $1 $2)"
	echo "cmd=$cmd"

	$SAR_EXEC_FILE $cmd
}

get_reg_from_cc() {
	[ -z "$1" -o "null" == "$1" ] && echo "cn" && return

	if [ "$1" == "FR" -o "$1" == "ES" -o "$1" == "IT" -o "$1" == "PL" -o "$1" == "NL" -o "$1" == "GB" \
		-o "$1" == "RO" -o "$1" == "CH" -o "$1" == "EE" -o "$1" == "SI" -o "$1" == "DE" ]; then
		echo "eu"
	elif [ "$1" == "IN" ]; then
		echo "in"
	elif [ "$1" == "TH" -o "$1" == "PH" -o "$1" == "MY" -o "$1" == "SG" -o "$1" == "ID" -o "$1" == "VN" -o "$1" == "MM" \
		-o "$1" == "NP" -o "$1" == "BD" -o "$1" == "LK" -o "$1" == "PK" -o "$1" == "AE" -o "$1" == "SA" -o "$1" == "NG" \
		-o "$1" == "EG" -o "$1" == "DZ" -o "$1" == "TN" -o "$1" == "MA" -o "$1" == "KE" -o "$1" == "KZ" -o "$1" == "RU" \
		-o "$1" == "AU" -o "$1" == "NZ" -o "$1" == "TW" -o "$1" == "JP" -o "$1" == "TR" -o "$1" == "KH" -o "$1" == "HK" \
		-o "$1" == "UA" ]; then
		echo "ce"
	elif [ "$1" == "MX" ]; then
		echo "fcc"
	fi
}

get_sar_idx() {
	idx="0"

	[ "$#" -ne "8" ] && echo "$idx" && return

	cc="$1"
	wifi2g="$2"
	wifi5g="$3"
	modem="$4"
	screen_on="$5"
	test_card="$6"
	sensor_near="$7"
	project="$8"
				
	reg=$(get_reg_from_cc "$cc")

	[ -z "$reg" ] && echo "$idx" && return

	if [ "eu" == "$reg" ]; then
			if [ "$wifi2g" == "1" -a "$modem" == "0" -a "$wifi5g" == "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="1"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="2"
				fi
			#5g only
			elif [ "$wifi2g" == "0" -a "$modem" == "0" -a "$wifi5g" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="3"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="4"
				fi
			#2.4g+5g
			elif [ "$wifi2g" == "1" -a "$modem" == "0" -a "$wifi5g" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="5"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="6"
				fi
			#2.4g+gsm
			elif [ "$wifi2g" == "1" -a "$wifi5g" == "0" -a "$modem" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="7"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="8"
				fi
			#5g+gsm
			elif [ "$wifi2g" == "0" -a "$wifi5g" -ne "0" -a "$modem" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="9"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="10"
				fi
			#2.4g+5g+gsm
			elif [ "$wifi2g" == "1" -a "$wifi5g" -ne "0" -a "$modem" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="11"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="12"
				fi
			fi
	# for ce
	elif [ "ce" == "$reg" ]; then
			# 2.4G only
			if [ "$wifi2g" == "1" -a "$modem" == "0" -a "$wifi5g" == "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="13"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="14"
				fi
			#5g only
			elif [ "$wifi2g" == "0" -a "$modem" == "0" -a "$wifi5g" != "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="15"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="16"
				fi
			#2.4g+5g
			elif [ "$wifi2g" == "1" -a "$modem" == "0" -a "$wifi5g" != "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="17"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="18"
				fi
			#2.4g+gsm
			elif [ "$wifi2g" == "1" -a "$wifi5g" == "0" -a "$modem" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="19"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="20"
				fi
			#5g+gsm
			elif [ "$wifi2g" == "0" -a "$wifi5g" != "0" -a "$modem" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="21"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="22"
				fi
			#2.4g+5g+gsm
			elif [ "$wifi2g" == "1" -a "$wifi5g" != "0" -a "$modem" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="23"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="24"
				fi
			fi
	# for fcc
	elif [ "fcc" == "$reg" ]; then
			# 2.4G only
			if [ "$wifi2g" == "1" -a "$modem" == "0" -a "$wifi5g" == "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="25"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="26"
				fi
			#5g only
			elif [ "$wifi2g" == "0" -a "$modem" == "0" -a "$wifi5g" != "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="27"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="28"
				fi
			#2.4g+5g
			elif [ "$wifi2g" == "1" -a "$modem" == "0" -a "$wifi5g" != "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="29"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="30"
				fi
			#2.4g+gsm
			elif [ "$wifi2g" == "1" -a "$wifi5g" == "0" -a "$modem" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="31"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="32"
				fi
			#5g+gsm
			elif [ "$wifi2g" == "0" -a "$wifi5g" != "0" -a "$modem" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="33"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="34"
				fi
			#2.4g+5g+gsm
			elif [ "$wifi2g" == "1" -a "$wifi5g" != "0" -a "$modem" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="35"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="36"
				fi
			fi
		elif [ "in" == "$reg" ]; then
			# 2.4G only
			if [ "$wifi2g" == "1" -a "$modem" == "0" -a "$wifi5g" == "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="37"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="38"
				fi
			#5g only
			elif [ "$wifi2g" == "0" -a "$modem" == "0" -a "$wifi5g" != "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="39"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="40"
				fi
			#2.4g+5g
			elif [ "$wifi2g" == "1" -a "$modem" == "0" -a "$wifi5g" != "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="41"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="42"
				fi
			#2.4g+gsm
			elif [ "$wifi2g" == "1" -a "$wifi5g" == "0" -a "$modem" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="43"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="44"
				fi
			#5g+gsm
			elif [ "$wifi2g" == "0" -a "$wifi5g" != "0" -a "$modem" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="45"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="46"
				fi
			#2.4g+5g+gsm
			elif [ "$wifi2g" == "1" -a "$wifi5g" != "0" -a "$modem" -ne "0" ]; then
				#head
				if [ "$sensor_near" == "1" ]; then
					idx="47"
				#body
				elif [ "$sensor_near" == "0" ]; then
					idx="48"
				fi
			fi
	fi

	echo "$idx"
}

get_real_project_version() {
	project_info_file="/vendor/etc/project_info.txt"
	real_project_name=""
	if [ -f "$project_info_file" ]; then
		first_line=$(head -1 "$project_info_file")
		file_list=(${first_line//,/ })
		len=${#file_list[@]}
		target_version=($(seq 0 $((len-1))))
		target_len=0
		if [ "$len" -ge "2" ]; then
			target_len=$((len-1))
			for i in $(seq 0 $((target_len-1))); do
				target_version[$i]=$(cat ${file_list[$i]})
			done
		fi

		for l in $(cat "$project_info_file"); do
			array=(${l//,/ })

			array_len=${#array[@]}
			if [ "$array_len" -eq "$len" ]; then
				for i in $(seq 0 $target_len); do
					if [ "${array[$i]}" != "${target_version[$i]}" ]; then
						break
					fi
				done

				if [ "$i" -eq "$target_len" ]; then
					real_project_name=${array[$i]}
					break
				fi
			fi
		done
	fi

	if [ -z "$real_project_name" ]; then
		real_project_name=$(cat /proc/oppoVersion/prjName)
	fi

	echo "$real_project_name"
}

project_version=$(get_real_project_version)
[ -z "$project_version" ] && return

# Note:
# 1. countryCode:wifi 2.4G enable(0/1):wifi 5G band(0-4):modem work type(0-3, disable/idle/4G/5G):screen on/off:isTestCard:SensorState.
sar_cmd=$(getprop "oppo.wifi.sar.idx" "")

cmd_array=(${sar_cmd//:/ })
array_len=${#cmd_array[@]}

[ "$array_len" -ne "7" ] && echo "Invalid sar cmd!" && return

country_code=${cmd_array[0]}
wifi2g=${cmd_array[1]}
wifi5g=${cmd_array[2]}
modem=${cmd_array[3]}
screen=${cmd_array[4]}
test_card=${cmd_array[5]}
sensor=${cmd_array[6]}

sarIdx=$(get_sar_idx "$country_code" "$wifi2g" "$wifi5g" "$modem" "$screen" "$test_card" "$sensor" "$project_version")

apply_sar "$sarIdx" "$project_version"
